package projectActivities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity4 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority=0)
	public void login() {

  driver.findElement(By.id("txtUsername")).sendKeys("orange");
  driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
  driver.findElement(By.id("btnLogin")).click();
  
	}
	
  @Test(priority=1)
  public void addEmployee() {
	  
  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  WebDriverWait wait = new WebDriverWait (driver, 20);
  
  wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_pim_viewPimModule")))); 
  WebElement PIM = driver.findElement(By.id("menu_pim_viewPimModule"));
  PIM.click();
  PIM.click();
  
  wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("btnAdd"))));
  WebElement add = driver.findElement(By.id("btnAdd"));
  add.click();
  
  driver.findElement(By.id("firstName")).sendKeys("Sam");
  driver.findElement(By.id("lastName")).sendKeys("Koala");
  driver.findElement(By.id("btnSave")).click();
  String employee = driver.findElement(By.tagName("h1")).getText();
  Assert.assertEquals("Sam Koala", employee);
 
  }
  

	@AfterClass
	public void afterClass() {
	driver.quit();
	}
}
