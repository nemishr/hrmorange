package projectActivities;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity11 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void addCandidate() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		Actions builder = new Actions(driver);

		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_recruitment_viewRecruitmentModule"))));
		WebElement recruitment = driver.findElement(By.id("menu_recruitment_viewRecruitmentModule"));
		recruitment.click();
		recruitment.click();

		WebElement addBtn = driver.findElement(By.id("btnAdd"));
		wait.until(ExpectedConditions.visibilityOf(addBtn));
		addBtn.click();

		driver.findElement(By.name("addCandidate[firstName]")).sendKeys("Harry");
		
		driver.findElement(By.name("addCandidate[lastName]")).sendKeys("Potter");
		
		driver.findElement(By.id("addCandidate_email")).sendKeys("potter.harry@gmail.com");
		
		File resume = new File("C:\\Users\\NehaMishra\\Desktop\\News.pdf");
		
		WebElement uploadFile = driver.findElement(By.name("addCandidate[resume]"));
		uploadFile.sendKeys(resume.getAbsolutePath());
		
		driver.findElement(By.id("btnSave")).click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		String heading = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div[1]/h1")).getText();
		Assert.assertEquals(heading, "Candidate's History");
		System.out.println("Candidate created successfully");

	}

	@AfterClass
	public void afterClass() {
		//driver.quit();
	}
}
