package projectActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity7 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void workex() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_pim_viewMyDetails"))));
		WebElement myInfo = driver.findElement(By.id("menu_pim_viewMyDetails"));
		myInfo.click();
		myInfo.click();
		
		driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[1]/ul/li[9]/a")).click();

		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("btnAdd"))));

		/*
		 * WebElement sidebarList =
		 * driver.findElement(By.xpath("//div[@id='sidebar']/ul")); List<WebElement>
		 * listItems = sidebarList.findElements(By.tagName("li")); for (WebElement li :
		 * listItems) { if (li.getText().equals("Qualifications")) { li.click(); } }
		 */

		WebElement addexp = driver.findElement(By.id("addWorkExperience"));
		wait.until(ExpectedConditions.visibilityOf(addexp));
		addexp.click();

		WebElement company = driver.findElement(By.id("experience_employer"));
		company.clear();
		company.sendKeys("IBM");

		WebElement jobTitle = driver.findElement(By.id("experience_jobtitle"));
		jobTitle.clear();
		jobTitle.sendKeys("System Engineer");

		driver.findElement(By.id("btnWorkExpSave")).click();
		System.out.println("Experience Added");

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
