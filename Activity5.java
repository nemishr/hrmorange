package projectActivities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity5 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void editMyinfo() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_pim_viewMyDetails"))));
		WebElement myInfo = driver.findElement(By.id("menu_pim_viewMyDetails"));
		myInfo.click();
		myInfo.click();

		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("btnAdd"))));
		WebElement edit = driver.findElement(By.id("btnSave"));
		edit.click();

		WebElement firstName = driver.findElement(By.id("personal_txtEmpFirstName"));
		firstName.clear();
		firstName.sendKeys("Neha");
		
		WebElement lastName = driver.findElement(By.id("personal_txtEmpLastName"));
		lastName.clear();		
		lastName.sendKeys("Mishra");

		driver.findElement(By.id("personal_optGender_2")).click();

		Select nationality = new Select(driver.findElement(By.id("personal_cmbNation")));
		nationality.selectByIndex(82);
		WebElement selected = nationality.getFirstSelectedOption();
		String country = selected.getText();
		System.out.println("The selected nationality is: " + country);
		
		WebElement dob = driver.findElement(By.id("personal_DOB"));
		dob.clear();
		dob.sendKeys("1994-11-26");
		
		driver.findElement(By.id("btnSave")).click();

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
