package projectActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity9 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void emergencyContacts() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_pim_viewMyDetails"))));
		WebElement myInfo = driver.findElement(By.id("menu_pim_viewMyDetails"));
		myInfo.click();
		myInfo.click();

		driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[1]/ul/li[3]/a")).click();

		List<WebElement> detailsheader = driver.findElements(By.xpath("//table[@id='emgcontact_list']/thead/tr[1]"));
		for (WebElement header : detailsheader) {
			System.out.println(header.getText());
		}

		List<WebElement> rows = driver.findElements(By.xpath("//table[@id='emgcontact_list']/tbody/tr"));
		for (int i = 1; i <= rows.size(); i++) {
			WebElement row = driver.findElement(By.xpath("//table[@id='emgcontact_list']/tbody/tr[" + i + "]"));
			System.out.println("Emergency Contact No. " + i + "is: " + row.getText());
		}

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
