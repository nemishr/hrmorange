package projectActivities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity6 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void clickDirectory() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// WebDriverWait wait = new WebDriverWait(driver, 20);

		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_pim_viewMyDetails"))));
		WebElement directory = driver.findElement(By.id("menu_directory_viewDirectory"));
		Boolean visible = directory.isDisplayed();
		Assert.assertSame(visible, true);

		Boolean canclick = directory.isEnabled();
		Assert.assertSame(canclick, true);

		directory.click();
		directory.click();

		String heading = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(heading, "Search Directory");

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
