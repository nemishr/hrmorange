package projectActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity8 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void applyLeave() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);

		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_dashboard_index"))));
		WebElement myInfo = driver.findElement(By.id("menu_dashboard_index"));
		myInfo.click();

		driver.findElement(By.xpath("//span[contains(text(),'Apply Leave')]")).click();

		Select leaveType = new Select(driver.findElement(By.id("applyleave_txtLeaveType")));
		leaveType.selectByValue("1");
		String selected = leaveType.getFirstSelectedOption().getText();
		System.out.println("The leave type selected is " + selected);

		WebElement fromDate = driver.findElement(By.id("applyleave_txtFromDate"));
		fromDate.clear();
		fromDate.sendKeys("2020-04-21");

		WebElement toDate = driver.findElement(By.id("applyleave_txtToDate"));
		toDate.clear();
		toDate.sendKeys("2020-04-22");

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement apply = driver.findElement(By.id("applyBtn"));
		apply.click();
		apply.click();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'fadable')]")));
		String successmsg = driver.findElement(By.xpath("//div[contains(@class, 'success')]")).getText();

		System.out.println("The leave applied is: " + successmsg);
		Reporter.log("The leave applied is: " + successmsg);

		if (successmsg != null) {

			WebElement mylvList = driver.findElement(By.xpath("//a[contains(@href, 'viewMyLeaveList')]"));
			wait.until(ExpectedConditions.visibilityOf(mylvList));
			mylvList.click();
		}
	}

	// driver.findElement(By.id("menu_leave_viewMyLeaveList")).click();

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
