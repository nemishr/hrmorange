package projectActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity2 {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test
	public void getImageURL() {

  WebElement headerImage = driver.findElement(By.tagName("img"));
		/* String url = headerImage.getAttribute("src"); */
  System.out.println(headerImage.getAttribute("src"));
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
