package projectActivities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity3 {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test
	public void login() {

  driver.findElement(By.id("txtUsername")).sendKeys("orange");
  driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
  driver.findElement(By.id("btnLogin")).click();
  String homePage = driver.findElement(By.id("welcome")).getText();
  Assert.assertEquals("Welcome Test1", homePage);

		/* System.out.println(homePage); */
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
