package projectActivities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity10 {
	WebDriver driver;
	WebDriver wait;

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();

		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority = 0)
	public void login() {

		driver.findElement(By.id("txtUsername")).sendKeys("orange");
		driver.findElement(By.id("txtPassword")).sendKeys("orangepassword123");
		driver.findElement(By.id("btnLogin")).click();

	}

	@Test(priority = 1)
	public void jobVacancy() {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		Actions builder = new Actions(driver);

		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.id("menu_recruitment_viewRecruitmentModule"))));
		WebElement recruitment = driver.findElement(By.id("menu_recruitment_viewRecruitmentModule"));
		recruitment.click();
		recruitment.click();

		driver.findElement(By.id("menu_recruitment_viewJobVacancy")).click();

		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("btnAdd"))));

		WebElement addvac = driver.findElement(By.id("btnAdd"));
		wait.until(ExpectedConditions.visibilityOf(addvac));
		addvac.click();

		Select jobTitle = new Select(driver.findElement(By.xpath("//select[@id='addJobVacancy_jobTitle']")));
		jobTitle.selectByValue("3");

		driver.findElement(By.xpath("//input[@name='addJobVacancy[name]']")).sendKeys("Testing123");

		WebElement hiringManager = driver.findElement(By.id("addJobVacancy_hiringManager"));
		hiringManager.sendKeys("A");
		WebElement target = driver.findElement(By.xpath("//li[@class='ac_even ac_over']"));
		builder.moveToElement(target).click().perform();

		driver.findElement(By.id("btnSave")).click();

		String heading = driver.findElement(By.tagName("h1")).getText();
		Assert.assertEquals(heading, "Edit Job Vacancy");
		System.out.println("Experience Added");

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}
